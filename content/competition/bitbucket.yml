# This file is not directly used, but is used as a template for new competition pages.
---
  data:
    competitor: BitBucket
    gitlab_coverage: 75
    competitor_coverage: 50
    subheading: BitBucket is a git based hosting service and collaboration tool, built for teams that offers management of code, control on the code, and collaboration of the developers. BitBucket is owned by Atlassian, and integrates with other Atlassian products, including Jira and Trello.
    comparison_table:
      - stage: Create
        features:
          - feature: Source Code Management
            gitlab:
              coverage: 100
              description: GitLab provides leading edge SCM, with advanced scaling and availability when deploying On-Prem.
              details: |
                * A GitLab group is a collection of projects, along with data about how users can access those projects. Each group has a project namespace (the same way that users do)
                * Each group is associated with a number of users, each of which has a level of permissions for the group’s projects and the group itself. These range from “Guest” (issues and chat only) to “Owner” (full control of the group, its members, and its projects). The types of permissions are too numerous to list here, but GitLab has a helpful link on the administration screen.
                * A GitLab project roughly corresponds to a single Git repository. Every project belongs to a single namespace, either a user or a group. If the project belongs to a user, the owner of the project has direct control over who has access to the project; if the project belongs to a group, the group’s user-level permissions will take effect.
                * GitLab includes support for hooks, both at a project or system level. For either of these, the GitLab server will perform an HTTP POST with some descriptive JSON whenever relevant events occur. This is a great way to connect Git repositories and GitLab instances to the rest of a development automation, such as CI servers, chat rooms, or deployment tools.
                * Contains Predefined Roles for Accessing Repositories and Groups
                * GitLab high availability is achieved by having full redundancy of all components and automatic failover (i.e. if a component fails, its counterpart automatically takes over).
                  * Multiple Application Server nodes with a load balancer (Premium)
                  * Gitaly cluster.
                  * Omnibus installation with database redundancy and automatic failover (PostgreSQL and Redis) failover (Premium)
                * Horizontal scaling is supported for App Server nodes without downtime
              improving_product_capabilities: |
                * [Support for enormous repositories in Git](https://gitlab.com/groups/gitlab-org/-/epics/773){data-ga-name="link to support for enormous repositories in git" data-ga-location="body"}
                * [GitLab CLI](https://gitlab.com/groups/gitlab-org/-/epics/7514){data-ga-name="link to gitlab cli" data-ga-location="body"}
              link_to_documentation: https://docs.gitlab.com/ee/user/project/repository/
            competitor:
              coverage: 100
              description: Bitbucket provides advanced SCM with all standard features required for an SCM. Atlasian’s main focus is the Bitbucket Cloud. Bitbucket Data Center is their on-premise version, but its future isn’t clear; Atlassian stopped selling the Bitbucket server and its support will end on 15 Feb 2024.
              details: |
                * A workspace is where users create repositories, collaborate on code, and organize different streams of work in a Bitbucket Cloud account.
                * Groups can help organize workspace members, specify permissions, and grant access to repositories across multiple members of a workspace.
                * There is no concept of nested sub-groups like GitLab has. Users can add many groups to a workspace but not sub-groups, which is less flexible and requires more work to manage permissions and teams.
                * GitLab provides better High Availability for each component while BitBucket does not. In BitBucket, the code as a git repo is stored in NFS, so if there is a failover or data loss, everything will be lost. In GitLab, users can build a Gitaly cluster to have copies of git repos, and there is no single point of failure.
              link_to_documentation:
          - feature: Code Review
            gitlab:
              coverage: 100
              description: GitLab provides a standard code review tool making it easy to perform code reviews. It lacks requiring reviews before merging and scheduled review notifications.
              details: |
                * Teams leave comments on merge requests, and make code suggestions that users can accept from the user interface.
                * When work is reviewed, team members can choose to accept or reject it.
                * Users can review merge requests from the GitLab interface.
                * When editing the Reviewers field in a new or existing merge request, GitLab displays the name of the matching approval rule below the name of each suggested reviewer. Code Owners are displayed as Codeowner without group detail.
                * If the GitLab Workflow VS Code extension is installed, merge requests can be reviewed in Visual Studio Code.
              improving_product_capabilities: |
                * [Merge conflicts in diffs](https://gitlab.com/groups/gitlab-org/-/epics/4893){data-ga-name="link to merge conflicts in diffs" data-ga-location="body"}
                * [Merge requests that require my attention](https://gitlab.com/groups/gitlab-org/-/epics/5331){data-ga-name="link to merge requests that require my attention" data-ga-location="body"}
                * [Track unread files in merge requests](https://gitlab.com/groups/gitlab-org/-/epics/1409){data-ga-name="link to track unread files in merge requests" data-ga-location="body"}
              link_to_documentation: https://docs.gitlab.com/ee/user/project/merge_requests/
            competitor:
              coverage: 75
              description: BitBucket provides code review, allows users to add reviewers in the pull request that can comment on lines of code and leave feedback, and approve the pull request.
              details: |
                * On a Premium plan, repository admins can prevent pull requests that don't have a certain number of approvals from being merged.
                * The code review functionality is basic, for example users can’t make code suggestions like they can within GitLab.

              link_to_documentation: https://support.atlassian.com/bitbucket-cloud/docs/review-code-in-a-pull-request/
          - feature: Wiki
            gitlab:
              coverage: 100
              description: GitLab provides a basic solution for creating wiki pages, even allowing wikis for a group.
              details: |
                * Every wiki is a separate Git repository, so wiki pages can be created in the web interface, or locally using Git.
                * GitLab wikis support Markdown, RDoc, AsciiDoc, and Org for content. Wiki pages written in Markdown support all Markdown features, and also provide some wiki-specific behavior for links.
                * Provides direct support for adding images
              improving_product_capabilities: |
                * [Decoupling the page title, slug, and filename](https://gitlab.com/groups/gitlab-org/-/epics/3192){data-ga-name="link to decoupling the page title, slug, and filename" data-ga-location="body"} to address issues related to renaming pages, case sensitivity, and redirects
                * [List child pages of directories](https://gitlab.com/gitlab-org/gitlab/-/issues/346941){data-ga-name="link to list child pages of directories" data-ga-location="body"}
              link_to_documentation: https://docs.gitlab.com/ee/user/project/wiki/
            competitor:
              coverage: 100
              description: Bitbucket provides a Wiki, private or public
              details: |
                * The wiki editor allows adding text in the following formats: Markdown, Creole, reStructuredText, Textfile. And Preview before saving.
                * Supports adding images.
                * Provides edit history of each page.
              link_to_documentation:
          - feature: Pages
            gitlab:
              coverage: 100
              description: GitLab provides a solution for hosting static web-pages which provides managed templates of different frameworks, automatic certificate management, and custom redirects.
              details: |
                * Use for any personal or business website.
                * Use any Static Site Generator (SSG) or plain HTML.
                * Create websites for projects, groups, or a user account.
                * Host a site on a separate GitLab instance or on GitLab.com for free.
                * Connect custom domains and TLS certificates.
                * Attribute any license to content.
                * Lot’s of samples provided for different frameworks
                * [Let’s Encrypt](https://docs.gitlab.com/ee/user/project/pages/custom_domains_ssl_tls_certification/lets_encrypt_integration.html){data-ga-name="link to let’s encrypt" data-ga-location="body"} integration
                * Allows for [access control](https://docs.gitlab.com/ee/user/project/pages/pages_access_control.html){data-ga-name="link to access control" data-ga-location="body"}
                * Allows [redirects](https://docs.gitlab.com/ee/user/project/pages/redirects.html){data-ga-name="link to redirects" data-ga-location="body"}
              improving_product_capabilities: |
                * Introducing a [caching strategy for the Pages internal API](https://gitlab.com/groups/gitlab-org/-/epics/7920){data-ga-name="link to caching strategy for the pages internal api" data-ga-location="body"}
                * Evaluating whether some or all of [Pages can be served from a CDN](https://gitlab.com/groups/gitlab-org/-/epics/6757){data-ga-name="link to pages can be served from a cdn" data-ga-location="body"}
                * Researching [other options](https://gitlab.com/groups/gitlab-org/-/epics/6729){data-ga-name="link to other options" data-ga-location="body"} like offloading SSL to NGINX or instituting reasonable limits on storage size and bandwidth
                * [Group level access control for Pages](https://gitlab.com/gitlab-org/gitlab/-/issues/254962){data-ga-name="link to group level access control for pages" data-ga-location="body"}
                * [Multiple version support](https://gitlab.com/gitlab-org/gitlab/-/issues/16208){data-ga-name="link to multiple version support" data-ga-location="body"}
                * [Use Pages without a DNS wildcard](https://gitlab.com/gitlab-org/gitlab/-/issues/17584){data-ga-name="link to use pages without a dns wildcard" data-ga-location="body"}
              link_to_documentation: https://docs.gitlab.com/ee/user/project/pages/
            competitor:
              coverage: 75
              description: BitBucket provides a solution for hosting static web-pages, but doesn’t support custom domains or certificates or pipelines to deploy the website.
              details: |
                * Bitbucket websites is a static website that contains coded HTML pages with fixed content.
                * While in GitLab you can use pipeline to deploy and publish the website, you can’t make it in Bitbucket, at least not in the on-premises version.
                * The website domain will be <workspaceID>.bitbucket.io - no support for custom domains.
                * The system does not issue cookies.
                * Server-side scripts or code are not supported. For example, PHP is not available.
                * Each page will be cached for 15 minutes. So your changes might not be visible immediately.
                * Each workspace can have only one site hosted on bitbucket.io.
              link_to_documentation: https://support.atlassian.com/bitbucket-cloud/docs/publishing-a-website-on-bitbucket-cloud/
          - feature: WebIDE
            gitlab:
              coverage: 75
              description: GitLab provides a new, robust Web IDE powered by Visual Studio Code, allowing for navigation, code changes, direct search for code, and terminal access with remote development, but lacks the ability to extend the Web IDE to manage issues and comments.
              details: |
                * The Web Integrated Development Environment (IDE) editor streamlines the process to contribute changes to projects, by providing an advanced editor with commit staging.
                * Syntax colorization for a variety of programming, scripting and markup languages such as XML, PHP, C#, C++, Markdown, Java, VB, Batch, Python, Ruby, and Objective-C.
                * Terminal access with remote development environment directly from the Web IDE.
              improving_product_capabilities: |
                * Provide access to a [runner terminal](https://docs.gitlab.com/ee/user/project/web_ide/index.html#interactive-web-terminals-for-the-web-ide){data-ga-name="link to runner terminal" data-ga-location="body"}
                * Provide [1st class editing experiences](https://gitlab.com/groups/gitlab-org/-/epics/2707){data-ga-name="link to 1st class editing experiences" data-ga-location="body"} for GitLab features.
              link_to_documentation: https://docs.gitlab.com/ee/user/project/web_ide/index.html
            competitor:
              coverage: 0
              details: |
                * Instead of a built-in web IDE, BitBucket offers integrations with 3rd parties cloud IDEs.
              link_to_documentation: https://support.atlassian.com/bitbucket-cloud/docs/install-cloud-ide-add-ons/
          - feature: Snippets
            gitlab:
              coverage: 100
              description: GitLab offers a solution for easily sharing code snippets with others, but limits being able to view revisions in the UI.
              details: |
                * Comment on, clone, and use version control in snippets.
                * They can contain multiple files.
                * They also support syntax highlighting, embedding, downloading, and users can maintain their snippets with the snippets API.
                * We offer an API to manage these snippets
                * A single snippet can support up to 10 files
              improving_product_capabilities: |
                * Add a way for developers to easily share and find common snippets, current [snippet search](https://gitlab.com/explore/snippets){data-ga-name="link to snippet search" data-ga-location="body"} is hard to search (Not Planned)
                * Allow [Snippet Revisions to be seen via UI](https://gitlab.com/gitlab-org/gitlab/-/issues/39271){data-ga-name="link to snippet revisions to be seen via ui" data-ga-location="body"}
                  * Developers may want to see changes, possible due to updates in the software the snippet reacts with
                * Support the ability to run [simple scripts](https://gitlab.com/groups/gitlab-org/-/epics/2397){data-ga-name="link to simple scripts" data-ga-location="body"} (e.g. python, ruby, shell) and [support html/css/js and javascript](https://gitlab.com/groups/gitlab-org/-/epics/2398){data-ga-name="link to support html/css/js and javascript" data-ga-location="body"} framework based demos.
                * Follow standard [embed](https://gitlab.com/groups/gitlab-org/-/epics/1496){data-ga-name="link to embed" data-ga-location="body"} capabilities so they can be used outside of GitLab easily
              link_to_documentation: https://docs.gitlab.com/ee/user/snippets.html
            competitor:
              coverage: 100
              description: Bitbucket offers Snippets to share code segments or files with the snippets owners, members of a workspace, or the world.
              details: |
                * Snippets are built on Git so revisions are tracked and users can clone and push.
                * They can contain multiple files.
                * Built for collaboration or individual sharing.
                * Rich set of APIs
                * Support for syntax highlighting.
                * You can drag files or paste a snippet of code.
                * Syntax sensitive, making drag and drop quick and simple.
              link_to_documentation: https://support.atlassian.com/bitbucket-cloud/docs/snippets-overview/
          - feature: Search
            gitlab:
              coverage: 100
              description: GitLab provides advanced search functionalities for searching through the site.
              details: |
                * Advanced Search uses Elasticsearch for faster, more advanced search across the entire GitLab instance.
                  * Projects
                  * Issues
                  * Merge requests
                  * Milestones
                  * Users
                  * Epics (when searching in a group only)
                  * Code
                  * Comments
                  * Commits
                  * Wiki (except group wikis)
                * Advanced Search can be useful in various scenarios:
                  * Faster searches: Advanced Search is based on Elasticsearch, which is a purpose-built full text search engine that can be horizontally scaled so that it can provide search results in 1-2 seconds in most cases.
                  * Code Maintenance: Finding all the code that needs to be updated at once across an entire instance can save time spent maintaining code. This is especially helpful for organizations with more than 10 active projects. This can also help build confidence in code refactoring to identify unknown impacts.
                  * Promote innersourcing: a company may consist of many different developer teams each of which has their own group where the various projects are hosted. Some applications may be connected to each other, so developers need to instantly search throughout the GitLab instance and find the code they search for.
                * Only available under premium
              improving_product_capabilities: |
                * Make generally available to enable better searching for OpenSource (Not Planned)
                * Create a visual interface for the search (Not Planned)
              link_to_documentation: https://docs.gitlab.com/ee/user/search/advanced_search.html
            competitor:
              coverage: 50
              description:  BitBucket provides Code aware search which analyzes your code syntax, ensuring definitions matching your search term are prioritized over usages and variable names.
              details: |
                * Code aware search, specifically built for teams who have many repos or large code bases.
                * It saves time by giving users the relevant results they want instead of the hassle of checking out a repo locally and searching using an IDE.
                * In Bitbucket Data Center and Server, you can easily find pull requests within a repository by using the search bar and filters available on the pull request page, including status, author, branch, reviewer, and text that is in the title or description
                * The search won’t work for content inside the pull request, for example comments.

              link_to_documentation: https://confluence.atlassian.com/bitbucketserver/bitbucket-search-syntax-814204781.html
          - feature: Forum / Discussion
            gitlab:
              coverage: 0
              link_to_documentation: https://docs.gitlab.com/ee/user/discussions/
            competitor:
              coverage: 0
              link_to_documentation: https://docs.github.com/en/discussions
          - feature: Remote Development
            gitlab:
              coverage: 25
              description: GitLab recently introduced a remote development category and a new web IDE powered by VS Code that can connect to a remote development.
              details: |
                * GitLab provides a remote development category, its maturity level is minima.
                * From the web IDE you can open terminal access to the remote environment, so developers have flexibility to run any command, like see logs, install dependencies and everything from their web browser without instaling or configuring anything on their local machine.
                * You can switch mode to use your Web IDE as a client and connect to the VS Code server installed on the remote environment.
              improving_product_capabilities: |
                * [Implement Remote Development](https://gitlab.com/groups/gitlab-org/-/epics/7948){data-ga-name="link to implement remote development" data-ga-location="body"}
            competitor:
              coverage: 0
              description: BitBucket does not offer remote development; users require GitPod for it.
              link_to_documentation: https://www.gitpod.io/blog/bitbucket
        overview_analysis: |
          Within basic features required for repository management, both GitLab and Bitbucket include a lot of similarities. Pull request, inline editing, code review, two factor authentication, markdown support, feature rich api, fork/clone repositories, wiki, third party integrations and sophisticated permissions management are all offered by both.

          However, out of the two, only GitLab offers an open-source version. GitLab is also a single DevSecOps platform, while BitBucket requires integrations for anything beyond the code management. GitLab provides better High Availability for each component while BitBucket does not. In BitBucket, the code as a git repo is stored in NFS, so if there is a failover or data loss, everything will be lost. In GitLab, users can build a Gitaly cluster to have copies of git repos, and there is no single point of failure. Another downside in BitBucket is that running git on NFS may cause performance issues, and is a known issue to Atlassian.
        gitlab_product_roadmap:
          - roadmap_item: Improved framework for source code management rules
          - roadmap_item: Support for enormous repositories in Git
          - roadmap_item: Track unread files in merge requests
