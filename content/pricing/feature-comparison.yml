---
  title: Self-Managed Feature Comparison
  description: On this page you can view information on Self-Managed Feature Comparison for GitLab. View more!
  components:
    - name: features-comparison-list
      data:
        title: Compare features
        caveat: '** Available on self-managed plans only'
        plans:
          - name: 'Free                        '
            price: $0 per user/month
            description: No credit card required
            button_text: Get Started
            button_variant: tertiary
            button_href: https://gitlab.com/-/trials/new?glm_content=default-saas-trial&glm_source=about.gitlab.com&_gl=1*nh1qyb*_ga*MzY4MDQ0NjU4LjE2NTIxMjM0Mjk.*_ga_ENFH3X7M5Y*MTY2MDc1MzIyOS4yMi4xLjE2NjA3NTg1OTAuMC4wLjA
          - name: Premium
            price: $19 per user/month
            description: Billed annually at $228 USD
            button_text: Buy Premium
            button_variant: primary
            button_href: https://gitlab.com/-/subscriptions/new?plan_id=2c92a00d76f0d5060176f2fb0a5029ff&test=capabilities
          - name: Ultimate
            price: $99 per user/month
            description: Billed annually at $1188 USD
            button_text: Buy Ultimate
            button_variant: primary
            button_href: https://gitlab.com/-/subscriptions/new?plan_id=2c92a0ff76f0d5250176f2f8c86f305a&test=capabilities
        blocks:
          - title: Source Code Management
            features:
              - name: Built-in CI/CD
                button_href: /features/continuous-integration/
                tooltip_text: |
                  GitLab has built-in Continuous Integration/Continuous Delivery, for free, no need to install it separately.
                  Use it to build, test, and deploy your website (GitLab Pages) or webapp.
                  The job results are displayed on merge requests for easy access.
                plans:
                  - 'free'
                  - 'premium'
                  - 'ultimate'
              - name: Publish static websites for free with GitLab Pages
                button_href: /stages-devops-lifecycle/pages/
                tooltip_text: |
                  GitLab Pages provides an easy system for hosting static sites using GitLab repositories and GitLab CI, complete with custom domains, access control, and HTTPS support.
                plans:
                  - 'free'
                  - 'premium'
                  - 'ultimate'
              - name: Container Scanning
                button_href: https://docs.gitlab.com/ee/user/application_security/container_scanning/
                tooltip_text: |
                  Scan container image dependencies against public vulnerability databases using the open source tools, Trivy or Grype.
                plans:
                  - 'free'
                  - 'premium'
                  - 'ultimate'
              - name: Push Rules
                button_href: https://docs.gitlab.com/ee/user/project/repository/push_rules.html
                tooltip_text: |
                  Reject new code and commits that don't comply with company policy.
                plans:
                  - 'premium'
                  - 'ultimate'
              - name: Multiple approvers in code review
                button_href: https://docs.gitlab.com/ee/user/project/merge_requests/approvals/rules.html
                tooltip_text: |
                  To ensure strict code review, you can require a minimum
                  number of users to approve of a merge request before it is able to be
                  merged.
                plans:
                  - 'premium'
                  - 'ultimate'
              - name: Efficient Merge Request reviews
                button_href: https://docs.gitlab.com/ee/user/project/merge_requests/approvals/settings
                tooltip_text: |
                  Guarantee quality and standards of your code by mandating a set number of necessary approvals and predefine a list of specific approvers.
                plans:
                  - 'premium'
                  - 'ultimate'
              - name: Code Quality Reports
                button_href: https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html#code-quality-reports
                tooltip_text: |
                  Full Code Quality reports are available on the pipeline page, showing areas of the codebase that do not meet the organization's preferred style or standards.
                plans:
                  - 'premium'
                  - 'ultimate'
              - name: Remote repository pull mirroring
                button_href: https://docs.gitlab.com/ee/user/project/repository/mirror/index.html
                tooltip_text: |
                  Mirror a repository to and from external sources. You can select which repository serves as the source, and modify which parts of the repository are copied. Branches, tags, and commits can be mirrored.
                plans:
                  - 'premium'
                  - 'ultimate'
              - name: Protected Environments
                button_href: https://docs.gitlab.com/ee/ci/environments/protected_environments.html
                tooltip_text: |
                  Specify which person, group, or account is allowed to deploy to a given
                  environment, allowing further protection and safety of sensitive environments.
                plans:
                  - 'premium'
                  - 'ultimate'
              - name: Merge Trains
                button_href: https://docs.gitlab.com/ee/ci/pipelines/merge_trains.html
                tooltip_text: |
                  Reduce pipeline queueing and waiting time with merge trains which allows parallel pipeline execution, with each pipeline building off the merge result of the previous one.
                plans:
                  - 'premium'
                  - 'ultimate'
              - name: Create test cases from within GitLab
                button_href: /direction/plan/#quality-management
                tooltip_text: |
                  Create and view test cases from within GitLab. This allows for seamless collaboration between contributors.
                plans:
                  - 'ultimate'
          - title: Project Management
            features:
              - name: Time Tracking
                button_href: /solutions/time-tracking/
                tooltip_text: |
                  Time Tracking in GitLab lets your team add estimates and record time spent on issues and merge requests.
                plans:
                  - 'free'
                  - 'premium'
                  - 'ultimate'
              - name: Wiki based project documentation
                button_href: https://docs.gitlab.com/ee/user/project/wiki/
                tooltip_text: |
                  A separate system for documentation called Wiki, is built right into each GitLab project. Every Wiki is a separate Git repository.
                plans:
                  - 'free'
                  - 'premium'
                  - 'ultimate'
              - name: Issue Weights
                button_href: https://docs.gitlab.com/ee/user/project/issues/issue_weight.html
                tooltip_text: |
                  GitLab lets you manage issues using Agile practices by setting the weight of an issue.
                plans:
                  - 'premium'
                  - 'ultimate'
              - name: Multiple Issue Assignees
                button_href: https://docs.gitlab.com/ee/user/project/issues/multiple_assignees_for_issues.html
                tooltip_text: |
                  Assign more than one person to an issue at a time.
                plans:
                  - 'premium'
                  - 'ultimate'
              - name: Roadmaps
                button_href: https://docs.gitlab.com/ee/user/group/roadmap/
                tooltip_text: |
                  Visualize multiple epics and milestones across time in a roadmap view.
                plans:
                  - 'premium'
                  - 'ultimate'
              - name: Code and Productivity Analytics
                button_href: https://docs.gitlab.com/ee/user/analytics/productivity_analytics.html
                tooltip_text: |
                  Productivity Analytics provides graphs and reports to help engineering leaders understand team, project,
                  and group productivity so they can uncover patterns and best practices
                  to improve overall productivity. The initial focus of Productivity Analytics is on the MR and how long it takes to merge MRs.
                plans:
                  - 'premium'
                  - 'ultimate'
              - name: Jira Development Panel Integration
                button_href: https://docs.gitlab.com/ee/integration/jira/issues.html
                tooltip_text: |
                  Extends the Jira integration, adding an option to display
                  a list of Jira issues natively inside the GitLab project, allowing developers
                  working primarily in GitLab to remain in flow, without having to reference
                  a second tool for tracking what work needs to get done.
                plans:
                  - 'premium'
                  - 'ultimate'
              - name: Single level Epics
                button_href: https://docs.gitlab.com/ee/user/group/epics/
                tooltip_text: |
                  Plan and track features and work group level epics that collect issues together. Easily create and assign Issues directly from the Epic itself
                plans:
                  - 'premium'
                  - 'ultimate'
              - name: Iterations
                button_href: https://docs.gitlab.com/ee/user/group/iterations/
                tooltip_text: |
                  Create and manage iterations at the group level, view all the issues for the iteration you’re currently working on
                  within your group or project, and enable all subgroups and projects to stay in sync on the same cadence.
                plans:
                  - 'premium'
                  - 'ultimate'
              - name: Quality Management
                button_href: /direction/plan/#quality-management
                tooltip_text: |
                  Define and plan test cases, maintain test execution results and create a backlog of work from failed tests.
                plans:
                  - 'ultimate'
              - name: Status Page
                button_href: https://docs.gitlab.com/ee/operations/incident_management/status_page.html
                tooltip_text: |
                  Deploy a static web page to communicate with stakeholders during an incident. Push updates to the Status Page directly from the incident.
                plans:
                  - 'ultimate'
          - title: Security and Compliance
            features:
              - name: Secret Detection
                button_href: https://docs.gitlab.com/ee/user/application_security/secret_detection/
                tooltip_text: |
                  GitLab allows you to perform Secret Detection in CI/CD pipelines; checking for unintentionally committed secrets and credentials. Results are then shown in the Merge Request and in the Pipeline view.
                  This feature is available as part of [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/) to provide security-by-default.
                plans:
                  - 'free'
                  - 'premium'
                  - 'ultimate'
              - name: Static Application Security Testing
                button_href: https://docs.gitlab.com/ee/user/application_security/sast/
                tooltip_text: |
                  GitLab allows easily running Static Application Security Testing (SAST) in CI/CD pipelines; checking for vulnerable source code or
                  well known security bugs in the libraries that are included by the application. Results are then shown in the Merge Request and in the Pipeline view.
                  This feature is available as part of [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/stages.html#auto-sast) to provide security-by-default.
                plans:
                  - 'free'
                  - 'premium'
                  - 'ultimate'
              - name: Verified Committer
                button_href: https://docs.gitlab.com/ee/user/project/repository/push_rules.html
                tooltip_text: |
                  Verify that a push only contains commits by the same user performing the push.
                plans:
                  - 'premium'
                  - 'ultimate'
              - name: Restrict push and merge access to certain users
                button_href: https://docs.gitlab.com/ee/user/project/protected_branches.html#restricting-push-and-merge-access-to-certain-users
                tooltip_text: |
                  Extend the base functionality of protected branches and choose which users can push or merge to a protected branch.
                plans:
                  - 'premium'
                  - 'ultimate'
              - name: Vulnerability Management
                button_href: /direction/govern/threat_insights/vulnerability_management/
                tooltip_text: |
                  Empower your entire team, and not just Security, to act on security findings with a
                  unified interface for scan results from all GitLab Security scanners.
                plans:
                  - 'ultimate'
              - name: Security Dashboards
                button_href: https://docs.gitlab.com/ee/user/application_security/security_dashboard/index.html
                tooltip_text: |
                  Gain visibility into top-priority fixes by identifying and tracking trends in security risk across your entire organization.
                plans:
                  - 'ultimate'
              - name: Dependency Scanning
                button_href: https://docs.gitlab.com/ee/user/application_security/dependency_scanning/
                tooltip_text: |
                  Protect your application from vulnerabilities that affect dynamic dependencies by automatically detecting well-known security bugs in your included libraries.
                plans:
                  - 'ultimate'
              - name: Project Dependency List
                button_href: https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#dependency-list
                tooltip_text: |
                  Identify components included in your project by accessing the Dependency List
                  (also referred to as Bill of Materials or BOM),
                  which is often requested by Security and Compliance teams.
                plans:
                  - 'ultimate'
              - name: Dynamic Application Security Testing
                button_href: https://docs.gitlab.com/ee/user/application_security/dast/
                tooltip_text: |
                  Ensure you are not exposed to web application vulnerabilities like broken authentication, cross-site scripting, or SQL injection by dynamically investigating your running test applications in CI/CD pipelines.
                plans:
                  - 'ultimate'
              - name: Compliance Dashboard
                button_href: https://docs.gitlab.com/ee/user/compliance/compliance_report/index.html
                tooltip_text: |
                  View an aggregated list of merge requests for all projects in a group. Easily identify and act on merge requests that are out of compliance or generate and export a chain of custody report for the group's projects.
                plans:
                  - 'ultimate'
              - name: Requirements Management
                button_href: /direction/plan/#requirements-management
                tooltip_text: |
                  Gather, document, refine, and track approval of business and system requirements.
                  Define traceability between requirements and other requirements, code, or
                  test cases.
                plans:
                  - 'ultimate'
          - title: Portfolio Management
            features:
              - name: Contributor Analytics
                button_href: https://docs.gitlab.com/ee/user/group/contribution_analytics/index.html
                tooltip_text: |
                  View contributions per member of a group
                plans:
                  - 'premium'
                  - 'ultimate'
              - name: Company-wide Portfolio Management
                button_href: /solutions/agile-delivery/
                tooltip_text: |
                  Plan and track work at the project and portfolio level. Manage capacity and resources together with Portfolio Management.
                plans:
                  - 'ultimate'
              - name: Issue and Epic Health Reporting
                button_href: https://docs.gitlab.com/ee/user/project/issues/index.html#health-status
                tooltip_text: |
                  Report on and quickly respond to the health of individual issues and epics by viewing red, amber, or green health statuses on your Epic Tree.
                plans:
                  - 'ultimate'
              - name: Portfolio-level Roadmaps
                button_href: https://docs.gitlab.com/ee/user/group/roadmap/
                tooltip_text: |
                  Establish product vision and strategy, gain progress insights, organize, govern and shape the effort of multi-disciplinary teams with portfolio-level roadmaps.
                plans:
                  - 'ultimate'
              - name: Group and Project Insights
                button_href: https://docs.gitlab.com/ee/user/project/insights/
                tooltip_text: |
                  Charts to visualize data such as triage hygiene, issues created/closed in a given period,
                  average time for merge requests to be merged and much more.
                plans:
                  - 'ultimate'
          - title: Scaled and Multi-Region Support
            features:
              - name: Dependency Proxy for Container Registry**
                button_href: https://docs.gitlab.com/ee/administration/geo/index.html
                tooltip_text: |
                  Supports distributed teams by running multiple registry instances across several regions and syncing between data centers.
                plans:
                  - 'free'
                  - 'premium'
                  - 'ultimate'
              - name: Globally distributed cloning with GitLab Geo**
                button_href: /solutions/geo/
                tooltip_text: |
                  Built for distributed teams, GitLab Geo helps to reduce time to clone and
                  fetch large repos with GitLab Geo - thereby speeding up the user experience for all users regardless of location.
                plans:
                  - 'premium'
                  - 'ultimate'
              - name: Disaster recovery**
                button_href: https://docs.gitlab.com/ee/administration/geo/disaster_recovery/index.html
                tooltip_text: |
                  Fail over in minutes to another data-center.
                plans:
                  - 'premium'
                  - 'ultimate'
              - name: Advanced Search
                button_href: https://docs.gitlab.com/ee/user/search/advanced_search.html
                tooltip_text: |
                  Provides faster, more advanced search across your entire GitLab instance.
                plans:
                  - 'premium'
                  - 'ultimate'
              - name: Support for Scaled GitLab**
                button_href: /support/#definition-of-scaled-architecture
                tooltip_text: |
                  Scale GitLab services across multiple nodes to manage demand
                  and provide redundancy. Determine the optimal architecture for your needs
                  using reference architectures.
                plans:
                  - 'premium'
                  - 'ultimate'
              - name: Packaged PostgreSQL with replication and failover**
                button_href: https://docs.gitlab.com/ee/administration/postgresql/replication_and_failover.html
                tooltip_text: |
                  Easily set up a PostgreSQL database cluster with automated failover.
                plans:
                  - 'premium'
                  - 'ultimate'
              - name: Advanced LDAP/SAML support including Group Sync**
                button_href: https://docs.gitlab.com/ee/administration/auth/ldap/index.html#group-sync
                tooltip_text: |
                  GitLab Premium (Self-Managed) gives your administrators the ability to automatically sync groups and manage SSH-keys, permissions,
                  and authentication, so you can focus on building your product, not configuring your tools.
                plans:
                  - 'premium'
                  - 'ultimate'
              - name: Import & Export Requirements
                button_href: /direction/plan/#requirements-management
                tooltip_text: |
                  To better collaborate with external groups and organizations, requirements can be imported and exported in CSV format.
                  This allows teams to use a single interface for development and testing against requirements.
                plans:
                  - 'ultimate'
