---
  title: Build the business case for GitLab
  description: ''
  side_menu:
    anchors:
      text: On this page
      data:
      - text: Audience
        href: "#audience"
        data_ga_name: audience
        data_ga_location: side-navigation
      - text: Requirements
        href: "#requirements"
        data_ga_name: requirements
        data_ga_location: side-navigation
      - text: ROI
        href: "#roi"
        data_ga_name: roi
        data_ga_location: side-navigation
      - text: Data
        href: "#data"
        data_ga_name: data
        data_ga_location: side-navigation
  hero:
    crumbs:
      - title: Get Started
        href: /get-started/
        data_ga_name: Get started
        data_ga_location: breadcrumb
      - title: Build the business case for GitLab
    header_label: 20 min to complete
    title: Build the business case for GitLab
    content: |
      Help your team get the DevSecOps platform it needs to deliver better software faster. Here are some tips for making the case for GitLab at your organization.
  copy_info:
  steps:
    groups:
      - header: Know your audience
        id: audience
        items:
          - title: Who are you building the business case for?
            copies:
              - text: |
                  Be clear on who will likely be reading and reviewing your business case, and who will need to sign off on the project. Always know who makes spending decisions and talk to them in their business/financial language.
          - title: How knowledgeable and enthusiastic are they about DevOps, security, CI/CD, etc?
            copies:
              - text: |
                  As you prepare your business case, keep in mind that the people reviewing it likely will have different levels of familiarity with terms and concepts like these. Don’t use acronyms or technical terms they may not understand.
          - title: What are their biggest challenges and pain points? 
            copies:
              - text: |
                  <p>Spend time thinking about — and potentially asking — what is most important and painful for the people who’ll be reviewing your business case. If it doesn’t address the things they care most about, they’ll be less likely to buy into what you’re saying. Here are a few ideas to get you started:</p>
                  <ul>
                  <li>Learn</li>
                  <li>Security issues</li>
                  <li>Slow or delayed deployments</li>
                  <li>Employee turnover and retention</li>
                  <li>Hiring challenges</li>
                  <li>Meeting customer needs</li>
                  </ul>
          - title: What are their goals or what might they like to accomplish with a DevSecOps platform?
            copies:
              - text: |
                  Are there any current company or department goals that GitLab would help them achieve? Do they have future business plans that GitLab’s DevSecOps platform could help them realize?
      - header: Understand the business problem and requirements
        id: requirements
        items:
          - title: Is there a specific initiative your organization could accomplish faster or better with GitLab?
            copies:
              - text: |
                  <p>For example, perhaps you need to:</p> 
                  <ul>
                  <li>Modernize a specific application or applications</li>
                  <li>Deliver a new critical application to the market</li>
                  <li>Transform or objectively improve your ability to deliver software</li>
                  <li>Modernize your DevOps capabilities</li>
                  <li>Reduce security and compliance risks </li>
                  <li>Increase operational efficiency by simplifying your development toolchain</li>
                  </ul>
          - title: What are your requirements for a DevSecOps platform?
            copies:
              - text: |
                  <p>Be sure to address your organization’s requirements for a DevSecOps platform, and how GitLab addresses them. </p>

                  <p>If you don’t have requirements, start with a value stream assessment to create them. This will look at your lines of business, the processes used to create and release software, and the time it takes to get it to a place where it can be used to generate revenue, retain existing customers, or generate business efficiencies. If you are interested in learning more about doing a value stream assessment with GitLab, <a href="https://about.gitlab.com/company/contact/">contact us</a>.</p>
      - header: Focus on expected business outcomes and ROI
        id: roi
        items:
          - title: Take time to think about and list the things your organization and team will be able to do with GitLab and the benefits you can expect. 
            copies:
              - text: |
                  <p>Here are some ideas to get you started: </p> 
                  <ul>
                  <li>Increased deployment speed, greater ability to hit release dates, etc.</li>
                  <li>Better security and easier compliance audits</li>
                  <li>Less time and effort spent maintaining a complex toolchain</li>
                  <li>Improved collaboration, breaking down departmental silos, etc.</li>
                  <li>More satisfied customers that stick around, buy more, and/or recommend you</li>
                  <li>Happier employees and better retention</li>
                  <li>Easier recruiting and onboarding</li>
                  </ul>
          - title: What return on investment (ROI) can your organization expect?
            copies:
              - text: |
                  <p>Do your best to lay out the return on investment (ROI) that your organization can expect when you adopt GitLab. This can include the time and money you’ll no longer have to spend maintaining a complex toolchain, and the various licensing fees you’ll no longer have to pay as you reduce your toolchain. You also may see increases in revenue and customer retention from releasing higher quality software more often.</p>

                  <p>Try our ROI calculator to see how much your current toolchain is costing you and how much you could save with GitLab.</p>
                link:
                  href: https://configure-roi-calc-abc-test.about.gitlab-review.app/calculator/roi/
                  text: Try the ROI calculator
                  ga_name: Try the ROI calculator
                  ga_location: body
      - header: Back up your business case with data and success stories 
        id: data
        items:
          - title: Find stats and data that speak to your organization’s pain points and goals 
            copies:
              - text: |
                  <p>Numbers can add credibility and urgency to your business case. Learning that X% of organizations are already doing something you’re not, or that organizations that are doing it are now able to do something they’ve always wanted to do Y times as often or faster, can be very motivating. Also look at other companies in your industry, as well as at your direct competitors, to see if they are saving money and increasing security and deployment speed with a DevSecOps platform. </p> 
                  <p>Here are some sources to get you started:</p>
                  <ul>
                  <li><a href="https://www.devops-research.com/research.html">DORA’s State of DevOps research program</a> and State of DevOps Reports</li>
                  <li><a href="https://about.gitlab.com/developer-survey/">2022 Global DecSecOps Survey</a>, as well as <a href="https://about.gitlab.com/developer-survey/previous/2021/">2021</a> and <a href="https://about.gitlab.com/developer-survey/previous/2020/">2020</a></li>
                  <li><a href="https://page.gitlab.com/resources-study-forrester-tei-gitlab-ultimate.html">2022 Forrester Consulting Total Economic Impact&trade; of GitLab's Ultimate Plan</a> study commissioned by GitLab</li>
                  </ul>
          - title: Show them how other organizations have done it
            copies:
              - text: |
                  <p>Seeing how a similar organization has achieved a similar goal can go a long way. Check out these case studies from a wide variety of GitLab customers — small businesses to enterprises, and in industries from financial services to retail and the public sector.</p>
                link:
                  href: https://about.gitlab.com/customers/
                  text: View Case Studies
                  ga_name: View case studies
                  ga_location: body

  resource_card_block:
    column_size: 4
    header_cta_text: View all resources
    header_cta_href: /resources/
    header_cta_ga_name: view all resources
    header_cta_ga_location: body
    cards:
      - icon:
          name: blog
          variant: marketing
          alt: Blog Icon
        event_type: "Blog"
        header: "The DevOps Platform series: Building a business case"
        link_text: "Read more"
        href: "/blog/2022/02/03/the-devops-platform-series-building-a-business-case/"
        image: "/nuxt-images/home/resources/Devops.png"
        alt: Winding path
        data_ga_name: "The DevOps Platform series"
        data_ga_location: "body"
      - icon:
          name: case-study
          alt: Case Study Icon
          variant: marketing
        event_type: "Case Study"
        header: "HackerOne achieves 5x faster deployments with GitLab’s integrated security"
        link_text: "Read more"
        href: "/customers/hackerone/"
        image: "/nuxt-images/blogimages/hackerone-cover-photo.jpg"
        alt: Hackerone Case Study
        data_ga_name: "HackerOne Case Study"
        data_ga_location: "body"
      - icon:
          name: report-alt
          variant: marketing
          alt: Report Icon
        event_type: "Report"
        header: "2022 Forrester Consulting Total Economic Impact ™️ of GitLab's Ultimate Plan"
        link_text: "Read more"
        href: "/resources-study-forrester-tei-gitlab-ultimate.html"
        image: "/nuxt-images/blogimages/Chorus_case_study.png"
        alt: Chorus case study
        data_ga_name: "2022 Forrester Consulting Total Economic Impact"
        data_ga_location: "body"
      - icon:
          name: blog
          variant: marketing
          alt: Blog Icon
        event_type: "Report"
        event_type: "Blog"
        header: "2022 Global DevSecOps Survey"
        link_text: "Read more"
        href: "/developer-survey/"
        image: "/nuxt-images/developer-survey/2022-devsecops-survey-landing-page-opengraph.jpeg"
        alt: City at night
        data_ga_name: "2022 Global DevSecOps Survey"
        data_ga_location: "body"
      - icon:
          name: case-study
          alt: Case Study Icon
          variant: marketing
        event_type: "Case Study"
        header: "Goldman Sachs improves from 1 build every two weeks to over a thousand per day"
        link_text: "Read more"
        href: "/customers/goldman-sachs/"
        image: "/nuxt-images/blogimages/Goldman_Sachs_case_study.jpg"
        alt: "Goldman Sachs Case Study"
        data_ga_name: "Goldman Sachs Case Study"
        data_ga_location: "body"
      - icon:
          name: web
          alt: Web Icon
          variant: marketing
        event_type: "Tool"
        header: "GitLab ROI calculator"
        link_text: "Try It"
        href: "/calculator/roi/"
        alt: "GitLab version number on a gradient"
        image: "/nuxt-images/resources/fallback/img-fallback-cards-infinity.png"
        data_ga_name: "GitLab ROI calculator"
        data_ga_location: "body"
