---
  title: Enterprise
  description: ''
  side_menu:
    anchors:
      text: On this page
      data:
      - text: Getting Started
        href: "#getting-started"
        data_ga_name: getting-started
        data_ga_location: side-navigation
      - text: Getting Setup
        href: "#getting-setup"
        data_ga_name: getting-setup
        data_ga_location: side-navigation
      - text: Using GitLab
        href: "#using-gitlab"
        data_ga_name: using-gitlab
        data_ga_location: side-navigation
  hero:
    crumbs:
      - title: Get Started
        href: /get-started/
        data_ga_name: Get Started
        data_ga_location: breadcrumb
      - title: Get Started for Enterprise
    header_label: 20 min to complete
    title: Get Started for Enterprise
    content: |
      To stay competitive, you need a way to simplify and scale DevOps so your teams can ship secure code faster. This guide will help you quickly set up the essentials for automated software development and delivery on the Premium tier with options to include security, compliance, and project planning found in the Ultimate tier.
  steps:
    groups:
      - header: Getting started
        id: getting-started
        items:
          - title: Determine which subscription is right for you
            copies:
              - title: GitLab SaaS or GitLab self-managed
                text: |
                  <p>Do you want GitLab to manage your GitLab platform or do you wish to manage it yourself?</p>
                link:
                  href: https://docs.gitlab.com/ee/subscriptions/#choose-between-gitlab-saas-or-gitlab-self-managed
                  text: See the differences
                  ga_name: GitLab Saas vs Self-Managed
                  ga_location: body
          - title: Determine which tier will meet your needs
            copies:
              - title: Premium or Ultimate
                text: |
                  <p>To determine which tier is right for you, consider the following: </p>

              - title: Amount of storage needed
                text: |
                  <p>Premium tier namespaces on GitLab SaaS have a 50GB storage limit and Ultimate tier namespaces go up to 250GB.</p>
              - title: Desired security and compliance
                text: |
                  <p>* Secrets detection, SAST, and container scanning are available in Premium.</p>
                  <p>* Additional scanners <a href="https://docs.gitlab.com/ee/user/application_security/"</a>such as DAST, dependencies, Cluster images, IaC, APIs, and fuzzing are available in Ultimate.</p>
                  <p>* Actionable findings, integrated into the merge request pipeline and the security dashboard require Ultimate for Vulnerability management.</p>
                  <p>* Compliance pipelines require Ultimate.</p>
                  <p>* Read about our security scanners <a href="https://docs.gitlab.com/ee/user/application_security/"</a> and our compliance capabilities<a href="https://docs.gitlab.com/ee/administration/compliance.html",/a>.
                link:
                  href: /pricing/
                  text: Visit our pricing to learn more
                  ga_name: pricing
                  ga_location: body
      - header: Getting set up
        id: getting-setup
        items:
          - title: Set up your SaaS subscription account
            copies:
              - title: Determine how many seats you want
                text: |
                  A GitLab SaaS subscription uses a concurrent (seat) model. You pay for a subscription according to the maximum number of users assigned to the top-level group or its children during the billing period. You can add and remove users during the subscription period, as long as the total users at any given time doesn’t exceed the subscription count.
                link:
                  href: https://docs.gitlab.com/ee/subscriptions/gitlab_com/index.html#how-seat-usage-is-determined
                  text: Learn more
                  ga_name: Determine how many seats you want
                  ga_location: body
              - title: Obtain your SaaS subscription
                text: |
                  <p>GitLab SaaS is the GitLab software-as-a-service offering, which is available at GitLab.com. You don’t need to install anything to use GitLab SaaS, you only need to sign up. The subscription determines which features are available for your private projects. Organizations with public open source projects can actively apply to our GitLab for Open Source Program.<p/>

                  <p>Features of <a href="/pricing/ultimate/">GitLab Ultimate</a>, including 50,000 CI/CD minutes, are free to qualifying open source projects through the <a href="/solutions/open-source/">GitLab for Open Source</a> Program.</p>
                link:
                  href: https://docs.gitlab.com/ee/subscriptions/gitlab_com/index.html#view-your-gitlabcom-subscription
                  text: Learn more
                  ga_name: Obtain your SaaS subscription
                  ga_location: body
              - title: Determine CI/CD shared runner minutes needed
                text: |
                  <p><a href="https://docs.gitlab.com/ee/ci/runners/runners_scope.html#shared-runners" data-ga-name="Shared Runners" data-ga-location="body">Shared runners</a> are shared with every project and group in a GitLab instance. When jobs run on shared runners, CI/CD minutes are used. On GitLab.com, the quota of CI/CD minutes is set for each <a href="https://docs.gitlab.com/ee/user/group/index.html#namespaces" data-ga-name="namespaces" data-ga-location="body">namespace</a>, and is determined by <a href="/pricing/" data-ga-name="Your license tier" data-ga-location="body">your license tier.</a><p/>

                  <p>In addition to the monthly quota, on GitLab.com, you can <a href="https://docs.gitlab.com/ee/ci/pipelines/cicd_minutes.html#purchase-additional-cicd-minutes" data-ga-name="purchase additional CI/CD minutes" data-ga-location="body">purchase additional CI/CD minutes</a> when needed.</p>
          - title: Set up your self-managed subscription account
            copies:
              - title: Determine how many seats you want
                text: |
                  A GitLab SaaS subscription uses a concurrent (seat) model. You pay for a subscription according to the maximum number of users assigned to the top-level group or its children during the billing period. You can add and remove users during the subscription period, as long as the total users at any given time doesn’t exceed the subscription count.
                link:
                  href: https://docs.gitlab.com/ee/subscriptions/self_managed/#subscription-seats
                  text: Learn more
                  ga_name: Determine how many seats you want
                  ga_location: body
              - title: Obtain your self-managed subscription
                text: |
                  You can install, administer, and maintain your own GitLab instance. GitLab subscription management requires access to the Customers Portal.
                link:
                  href: https://docs.gitlab.com/ee/subscriptions/self_managed/
                  text: Learn more
                  ga_name: Determine how many seats you want
                  ga_location: body
              - title: Activate GitLab Enterprise Edition
                text: |
                  When you install a new GitLab instance without a license, only Free features are enabled. To enable more features in GitLab Enterprise Edition (EE), activate your instance with an activation code.
                link:
                  href: https://docs.gitlab.com/ee/user/admin_area/license.html
                  text: Learn more
                  ga_name: Activate GitLab Enterprise Edition
                  ga_location: body
              - title: Review the system requirements
                text: |
                  Review both the supported operating systems and the minimum requirements needed to install and use GitLab.
                link:
                  href: https://docs.gitlab.com/ee/install/requirements.html
                  text: Learn more
                  ga_name: Review the system requirements
                  ga_location: body
              - title: Install GitLab
                text: |
                  <p>Choose your <a href="https://docs.gitlab.com/ee/install/#choose-the-installation-method" data-ga-name="Installation Method" data-ga-location="body">installation method</a></p>
                  <p>Install on <a href="https://docs.gitlab.com/ee/install/#install-gitlab-on-cloud-providers" data-ga-name="your cloud provider" data-ga-location="body">your cloud provider</a> (if applicable)</p>
              - title: Configure your instance
                text: |
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. In pellentesque enim sit amet euismod malesuada.
                link:
                  href: https://docs.gitlab.com/ee/install/next_steps.html
                  text: Learn more
                  ga_name: Configure your instance
                  ga_location: body
              - title: Set up off-line environment
                text: |
                  Set up off-line environment  when isolation from the public internet is required (typically applicable to regulated industries)
                link:
                  href: https://docs.gitlab.com/ee/user/application_security/offline_deployments/index.html
                  text: Learn more
                  ga_name: Set up off-line environment
                  ga_location: body
              - title: Consider limiting CI/CD shared runner minutes allowed
                text: |
                  To control resource utilization on self-managed GitLab instances, the quota of CI/CD minutes for each namespace can be set by administrators.
                link:
                  href: https://docs.gitlab.com/ee/ci/pipelines/cicd_minutes.html#set-the-quota-of-cicd-minutes-for-a-specific-namespace
                  text: Learn more
                  ga_name: Consider limiting CI/CD shared runner minutes allowed
                  ga_location: body
              - title: Consider limiting CI/CD shared runner minutes allowed
                text: |
                  To control resource utilization on self-managed GitLab instances, the quota of CI/CD minutes for each namespace can be set by administrators.
                link:
                  href: https://docs.gitlab.com/ee/ci/pipelines/cicd_minutes.html#set-the-quota-of-cicd-minutes-for-a-specific-namespace
                  text: Learn more
                  ga_name: Consider limiting CI/CD shared runner minutes allowed
                  ga_location: body
              - title: Install GitLab runner
                text: |
                  GitLab Runner can be installed and used on GNU/Linux, macOS, FreeBSD, and Windows.
                link:
                  href: https://docs.gitlab.com/runner/install/
                  text: Learn more
                  ga_name: Install GitLab runner
                  ga_location: body
              - title: Configure GitLab runner (optional)
                text: |
                  GitLab Runner can be installed and used on GNU/Linux, macOS, FreeBSD, and Windows.
                link:
                  href: https://docs.gitlab.com/runner/configuration/
                  text: Learn more
                  ga_name: Configure GitLab runner
                  ga_location: body
              - title: Adminstration
                text: |
                  Self-managed requires self-administration
                link:
                  href: https://docs.gitlab.com/ee/administration/
                  text: Learn more
                  ga_name: Self Administration
                  ga_location: body
          - title: Integrate applications (optional)
            copies:
              - text: |
                  You can add functionality such as secrets management or authentication services, or integrate incumbent applications such as issue trackers.
                link:
                  href: https://docs.gitlab.com/ee/integration/
                  text: Learn more
                  ga_name: Integrate applications
                  ga_location: body
      - header: Using GitLab
        id: using-gitlab
        items:
          - title: Set up your organization
            copies:
              - text: |
                  Configure your organization and its users. Determine user roles and give everyone access to the projects they need.
                link:
                  href: https://docs.gitlab.com/ee/topics/set_up_organization.html
                  text: Learn more
                  ga_name: Setup your organization
                  ga_location: body
          - title: Organize work with projects
            copies:
              - text: |
                  In GitLab, you can create projects to host your codebase. You can also use projects to track issues, plan work, collaborate on code, and continuously build, test, and use built-in CI/CD to deploy your app.
                link:
                  href: https://docs.gitlab.com/ee/user/project/index.html
                  text: Learn more
                  ga_name: Organize work with projects
                  ga_location: body
          - title: Plan and track work
            copies:
              - text: |
                  Plan your work by creating requirements, issues, and epics. Schedule work with milestones and track your team’s time. Learn how to save time with quick actions, see how GitLab renders Markdown text, and learn how to use Git to interact with GitLab.
                link:
                  href: https://docs.gitlab.com/ee/topics/plan_and_track.html
                  text: Learn more
                  ga_name: Plan and track work
                  ga_location: body
          - title: Build your application
            copies:
              - text: |
                  Add your source code to a repository, create merge requests to check in code, and use CI/CD to generate your application.
                link:
                  href: https://docs.gitlab.com/ee/topics/build_your_application.html
                  text: Learn more
                  ga_name: Build your application
                  ga_location: body
          - title: Secure your application
            copies:
              - title: Determine which scanners you’d like to use (all are on by default)
                text: |
                  GitLab offers both Container Scanning and Dependency Scanning to ensure coverage for all of these dependency types. To cover as much of your risk area as possible, we encourage you to use all of our security scanners.
                link:
                  href: https://docs.gitlab.com/ee/user/application_security/configuration/
                  text: Learn more
                  ga_name: Determine which scanners you’d like to use
                  ga_location: body
              - title: Configure your security policies
                text: |
                  Policies in GitLab provide security teams a way to require scans of their choice to be run whenever a project pipeline runs according to the configuration specified. Security teams can therefore be confident that the scans they set up have not been changed, altered, or disabled.
                link:
                  href: https://docs.gitlab.com/ee/user/application_security/policies/
                  text: Learn more
                  ga_name: Configure your security policies
                  ga_location: body
              - title: Configure MR approval rules and security approvals
                text: |
                  With merge request approval rules, you can set the minimum number of required approvals before work can merge into your project. You can also extend these rules to define what types of users can approve work.
                link:
                  href: https://docs.gitlab.com/ee/user/project/merge_requests/approvals/
                  text: Learn more
                  ga_name: Configure MR approval rules and security approvals
                  ga_location: body
          - title: Deploy and release your application
            copies:
              - text: |
                  Deploy your application internally or to the public. Use flags to release features incrementally.
                link:
                  href: https://docs.gitlab.com/ee/topics/release_your_application.html
                  text: Learn more
                  ga_name: Deploy and release your application
                  ga_location: body
          - title: Monitor application performance
            copies:
              - text: |
                  GitLab provides a variety of tools to help operate and maintain your applications.  You can track the metrics that matter most to your team, generate automated alerts when performance degrades, and manage those alerts - all within GitLab.
                link:
                  href: https://docs.gitlab.com/ee/operations/index.html
                  text: Learn more
                  ga_name: Monitor application performance
                  ga_location: body
          - title: Monitor runner performance
            copies:
              - text: |
                  GitLab comes with its own application performance measuring system as of GitLab 8.4, called “GitLab Performance Monitoring”. GitLab Performance Monitoring makes it possible to measure a wide variety of statistics
                link:
                  href: https://docs.gitlab.com/runner/monitoring/index.html
                  text: Learn more
                  ga_name: Monitor runner performance
                  ga_location: body
          - title: Manage your infrastructure
            copies:
              - text: |
                  With the rise of DevOps and SRE approaches, infrastructure management becomes codified, automatable, and software development best practices gain their place around infrastructure management too. GitLab offers various features to speed up and simplify your infrastructure management practices.
                link:
                  href: https://docs.gitlab.com/ee/user/infrastructure/index.html
                  text: Learn more
                  ga_name: Manage your infrastructure
                  ga_location: body
          - title: Analyze GitLab usage
            copies:
              - text: |
                  This measures how often you deliver value to end users. A higher deployment frequency means you can get feedback sooner and iterate faster to deliver improvements and features.
                link:
                  href: https://docs.gitlab.com/ee/user/analytics/index.html
                  text: Learn more
                  ga_name: Analyze GitLab usage
                  ga_location: body
  next_steps:
    header: Take your Enterprise to the next step
    cards:
      - title: Do you have a paid tier?
        text: You have a Technical Account Manager (TAM) support.
        avatar: /nuxt-images/icons/avatar_orange.png
        col_size: 4
        link:
          text: Have my TAM contact me
          url: /sales/
          data_ga_name: Have my TAM contact me
          data_ga_location: body
      - title: Need more help?
        text: GitLab Professional Services can help you get started, integrate with third party applications, and more
        avatar: /nuxt-images/icons/avatar_pink.svg
        col_size: 4
        link:
          text: Have my PS contact me
          url: /sales/
          data_ga_name: Have my PS contact me
          data_ga_location: body
      - title: Prefer to work with a channel partner?
        avatar: /nuxt-images/icons/avatar_blue.svg
        col_size: 4
        link:
          text: See channel partner directory
          url: /sales/
          data_ga_name: See channel partner directory
          data_ga_location: body
      - title: Considering an upgrade?
        text: Learn more about the benefits of [Premium](/pricing/premium/){data-ga-name="why premium" data-ga-location="body"} and [Ultimate](/pricing/ultimate/){data-ga-name="why ultimate" data-ga-location="body"}.
        col_size: 6
        link:
          text: See tiering details
          url: /sales/
          data_ga_name: See tiering details
          data_ga_location: body
      - title: Considering a third party integration?
        text: GitLab Professional Services can help you get started, integrate with third party applications, and more.
        col_size: 6
        link:
          text: See our Alliance and Technology partners
          url: /partners/
          data_ga_name: See our Alliance and Technology partners
          data_ga_location: body
